package com.company;

//      Zad 1. Utworzyć losową tablicę liczb < 10, 20 > zawierająca 20 elementów. a) ile wynosi rożnica średnich dla
//         liczb parzystych i nieparzystych b) ile liczb ma parzystą liczbę wystąpień
//
//        Zad 2. Wylosować tablicę zawierającą 3 liczby parzyste i 7 nieparzystych < 100
//
//        Zad 3. Wydawanie kwoty (reszty) bez ograniczeń (wszystkie nominały dostępne w nieograniczonej ilości)
//
//        Zad 4. Wydawanie kwoty (reszty) z ograniczoną dostępnością nominałów (każdy z nominałów dostępny w określonej ilości)

import java.math.BigDecimal;
import java.util.*;

public class Lab004 {
    //Zad 1. Utworzyć losową tablicę liczb < 10, 20 > zawierająca 20 elementów. a) ile wynosi rożnica średnich dla
// liczb parzystych i nieparzystych b) ile liczb ma parzystą liczbę wystąpień
    public void zad1() {
        int[] tab = new int[20];
        Random random = new Random();
        double sumaParzyste = 0;
        double sumaNieparzyste = 0;
        int licznik_parzystych_wystapien = 0;
        int licznikparzyste=0;
        int liczniknieparzyste=0;
        int[] tabwystapien = new int[21];

        for(int i=10;i<tabwystapien.length;i++){
            tabwystapien[i]=0;
        }
        for (int i = 0; i < 20; i++) {
            tab[i] = random.nextInt(21 - 10) + 10;
            tabwystapien[tab[i]]++;
            if (tab[i] % 2 == 0) {
                sumaParzyste = sumaParzyste + tab[i];
                licznikparzyste++;

            } else {
                sumaNieparzyste = sumaNieparzyste + tab[i];
                liczniknieparzyste++;
            }
        }
        for(int i=10;i<tabwystapien.length;i++){
            if(tabwystapien[i]%2==0){
                licznik_parzystych_wystapien++;
            }
        }
        double wynik =  (sumaParzyste/licznikparzyste) - (sumaNieparzyste/liczniknieparzyste);
        System.out.println("Roznica srednich liczb parzystych i nie parzystych wynosi " + wynik + " parzysta liczbe wystapien posiada " + licznik_parzystych_wystapien);
    }

    //  Zad 2. Wylosować tablicę zawierającą 3 liczby parzyste i 7 nieparzystych < 100

    public void zad2() {
        int[] tab = new int[10];
        Random random = new Random();
        int liczby_parzyste = 0;
        int liczby_nieparzyste = 0;

        for (int i = 0; i < 10; ) {
            tab[i] = random.nextInt(100);
            if (tab[i] % 2 == 0 && liczby_parzyste < 3) {
                liczby_parzyste++;
                i++;
            }
            if (tab[i] % 2 != 0 && liczby_nieparzyste < 7) {
                liczby_nieparzyste++;
                i++;
            }
        }
        for (int j : tab) {
            System.out.println(j);
        }
    }

    //Zad 3. Wydawanie kwoty (reszty) bez ograniczeń (wszystkie nominały dostępne w nieograniczonej ilości)

    public void zad3(BigDecimal reszta) {

        BigDecimal[] lista = new BigDecimal[15];
        ArrayList<BigDecimal> listawynik = new ArrayList<>();

        lista[0] = new BigDecimal("500.00");
        lista[1] = new BigDecimal("200.00");
        lista[2] = new BigDecimal("100.00");
        lista[3] = new BigDecimal("50.00");
        lista[4] = new BigDecimal("20.00");
        lista[5] = new BigDecimal("10.00");
        lista[6] = new BigDecimal("5.00");
        lista[7] = new BigDecimal("2.00");
        lista[8] = new BigDecimal("1.00");
        lista[9] = new BigDecimal("0.50");
        lista[10] = new BigDecimal("0.20");
        lista[11] = new BigDecimal("0.10");
        lista[12] = new BigDecimal("0.05");
        lista[13] = new BigDecimal("0.02");
        lista[14] = new BigDecimal("0.01");
        BigDecimal valueWhile = new BigDecimal("0");

        while ((reszta.compareTo(valueWhile)) == 1) {
            BigDecimal n = BigDecimal.valueOf(0);
            for (BigDecimal bigDecimal : lista) {
                //lista[i]<= reszta && lista[i]>0
                if ((bigDecimal.compareTo(reszta) == -1 || bigDecimal.compareTo(reszta) == 0) && bigDecimal.compareTo(valueWhile) == 1) {
                    n = bigDecimal;
                    break;
                }
            }
            reszta = reszta.subtract(n);
            listawynik.add(n);
        }
        wyswietl_Jaki_Nominal(listawynik);
    }

    //        Zad 4. Wydawanie kwoty (reszty) z ograniczoną dostępnością nominałów (każdy z nominałów dostępny w określonej ilości)

    public void zad4(BigDecimal reszta){

        LinkedHashMap<BigDecimal,Integer> stannominalow = new LinkedHashMap<>();
        ArrayList<BigDecimal> listawynik = new ArrayList<>();

        stannominalow.put(new BigDecimal("500.00"), 0);
        stannominalow.put(new BigDecimal("200.00"),2);
        stannominalow.put(new BigDecimal("100.00"),1);
        stannominalow.put(new BigDecimal("50.00"),3);
        stannominalow.put(new BigDecimal("20.00"),2);
        stannominalow.put(new BigDecimal("10.00"),1);
        stannominalow.put(new BigDecimal("5.00"),30);
        stannominalow.put(new BigDecimal("2.00"),4);
        stannominalow.put(new BigDecimal("1.00"),2);
        stannominalow.put(new BigDecimal("0.50"),3);
        stannominalow.put(new BigDecimal("0.20"),3);
        stannominalow.put(new BigDecimal("0.10"),2);
        stannominalow.put(new BigDecimal("0.05"),1);
        stannominalow.put(new BigDecimal("0.02"),3);
        stannominalow.put(new BigDecimal("0.01"),3);

        BigDecimal valueWhile = new BigDecimal("0");

        while ((reszta.compareTo(valueWhile)) == 1) {
             BigDecimal[] n = {BigDecimal.valueOf(0)};

             for(Map.Entry<BigDecimal,Integer> entry: stannominalow.entrySet()){
                 BigDecimal tmp = entry.getKey();
                 Integer counter =entry.getValue();

                 //lista[i]<= reszta && lista[i]>0
                 if ((tmp.compareTo(reszta) == -1 || tmp.compareTo(reszta) == 0) && tmp.compareTo(valueWhile) == 1 && counter>0) {
                    n[0] = tmp;
                    counter--;
                    stannominalow.put(tmp,counter);
                    break;
                 }
             }
            reszta = reszta.subtract(n[0]);
            listawynik.add(n[0]);
        }
        wyswietl_Jaki_Nominal(listawynik);
    }

    //metoda pomocnicza do zad  3 - 4 do wyswietlania wyniku
    private void wyswietl_Jaki_Nominal(ArrayList<BigDecimal> listawynik) {
        int counterResult =1;
        for(int i=0;i<listawynik.size();i++){
            if((i+1)< listawynik.size() && listawynik.get(i).compareTo(listawynik.get(i+1)) ==0 ){
                counterResult++;
            }
            else {
                System.out.println(listawynik.get(i) +" * " + counterResult);
                counterResult=1;
            }
        }
    }
}

